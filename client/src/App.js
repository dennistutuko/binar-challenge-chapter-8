import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div>
      <form action="">
        <label htmlFor="create-player">Create new player</label>
        <br/>
        <input type="text" name="create-player"/>
        <input type="button" value="Submit"/>
      </form>
      <br/>
      <form action="">
        <label htmlFor="create-player">Edit player</label>
        <br/>
        <input type="text" name="edit-player"/>
        <input type="button" value="Submit"/>
      </form>
      <br/>
      <form action="">
        <label htmlFor="find-player">Find player</label>
        <br/>
        <input type="text" name="find-player"/>
        <select name="category" id="">
          <option value="username">username</option>
          <option value="email">email</option>
          <option value="experience">experience</option>
          <option value="lvl">lvl</option>
        </select>
        
        <input type="button" value="Submit"/>
      </form>
    </div>
  );
}

export default App;
